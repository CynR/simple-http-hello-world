# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 
## Lancer Docker

Génerer Dockerfile :
```
FROM maven:3.6.3-jdk-13 as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn clean package
                      
FROM openjdk:13.0-slim
COPY --from=build /src/target/*.jar ./
                      
EXPOSE 8181
CMD java -jar *.jar
```
Exécuter depuis line de command :
```
- Etre sur le dossier du projet
- Ecrire:  docker build -t **myhttp** .  
                        **myhttp** = nom de l'image
- Ecrire:  docker run -it -d -p 8181:8181 -t **myhttp**
```