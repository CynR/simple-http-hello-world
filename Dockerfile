FROM maven:3.6.3-jdk-13 as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn clean package

FROM openjdk:13.0-slim
COPY --from=build /src/target/*.jar ./

EXPOSE 8181
CMD java -jar *.jar

